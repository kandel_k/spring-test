package com.example.demo.controller;

import com.example.demo.dto.UserSaveRequest;
import com.example.demo.model.Roles;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static com.example.demo.controller.UserControllerITWithServices.convertObjectToJsonBytes;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("demo")
public class UserControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenCreateUserThatNotExist_thenResponseOk() throws Exception {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.USER;
        saveRequest.username = "test";

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.username").isString())
                .andExpect(jsonPath("$.role").isString())
                .andExpect(jsonPath("$.tasks").isEmpty());
    }

    @Test
    public void whenCreateAdminByPersonThatNotExists_thenForbiddenStatus() throws Exception {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "test";
        saveRequest.creatorId = 10L;

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(status().reason("You are not allowed to do that"));
    }

    @Test
    public void whenGetAll_thenReturnValidResponse() throws Exception {
        mockMvc
                .perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$[0].username").isString())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].tasks").isArray())
                .andExpect(jsonPath("$[0].role").isString());
    }
}
