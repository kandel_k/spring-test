package com.example.demo.controller;

import com.example.demo.dto.UserSaveRequest;
import com.example.demo.model.Roles;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@WebMvcTest(UserController.class)
@Import(UserService.class)
public class UserControllerITWithServices {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @MockBean
    private UserRepository userRepository;

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void whenGetAll_thenReturnValidResponse() throws Exception {
        var user = new UserEntity(0L, "test", Roles.USER, Collections.emptyList());
        when(userRepository.findAll()).thenReturn(Collections.singletonList(user));

        mockMvc
                .perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].username").value("test"))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].tasks").isEmpty());
    }

    @Test
    public void whenCreateUserThatNotExist_thenResponseOk() throws Exception {
        var userEntity = new UserEntity(1L, "kandel_k", Roles.USER, Collections.emptyList());
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.USER;
        saveRequest.username = "kandel_k";

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(userEntity);

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(userEntity.getId()))
                .andExpect(jsonPath("$.username").value(userEntity.getUsername()))
                .andExpect(jsonPath("$.role").value(userEntity.getRole().name()))
                .andExpect(jsonPath("$.tasks").isEmpty());
    }

    @Test
    public void whenCreateUserThatExists_thenBadRequestStatus() throws Exception {
        var user = new UserEntity();
        var saveRequest = new UserSaveRequest();
        saveRequest.username = "kandel_k";
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("User already exists"));
    }

    @Test
    public void whenCreateAdminByPersonThatNotExists_thenForbiddenStatus() throws Exception {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andExpect(status().isForbidden())
                .andExpect(status().reason("You are not allowed to do that"));
    }

    @Test
    public void whenCreateAdminByUser_thenForbiddenStatus() throws Exception {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        var user = new UserEntity();
        user.setRole(Roles.USER);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andExpect(status().isForbidden())
                .andExpect(status().reason("You are not allowed to do that"));
    }

    @Test
    public void whenCreateAdminByAdmin_thenStatusOk() throws Exception {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        var userEntity = new UserEntity(1L, "kandel_k", Roles.ADMIN, Collections.emptyList());
        var admin = new UserEntity();
        admin.setRole(Roles.ADMIN);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(admin));
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(userEntity);

        mockMvc
                .perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJsonBytes(saveRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.username").isString())
                .andExpect(jsonPath("$.role").isString())
                .andExpect(jsonPath("$.tasks").isArray())
                .andExpect(jsonPath("$.tasks").isEmpty());
    }

    @Test
    public void whenUserDeleteHimself_thenResponseOk() throws Exception {
        mockMvc
                .perform(delete("/user/1").param("ownerId", "1"))
                .andExpect(status().isOk());
    }

    @Test
    public void whenUserDeleteNotHimself_thenForbiddenStatus() throws Exception {
        var user = new UserEntity();
        user.setRole(Roles.USER);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        mockMvc
                .perform(delete("/user/1").param("ownerId", "0"))
                .andExpect(status().isForbidden())
                .andExpect(status().reason("You are not allowed to do that"));
    }

    @Test
    public void whenAdminDelete_thenStatusOk() throws Exception {
        var user = new UserEntity();
        user.setRole(Roles.ADMIN);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        mockMvc
                .perform(delete("/user/1").param("ownerId", "1"))
                .andExpect(status().isOk());
    }

    /*
        Here should be IT for addTask controller, but I won't do them cause logic is very simple and unit tests
        checked everything
    */

}
