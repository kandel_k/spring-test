package com.example.demo.service;


import com.example.demo.dto.UserSaveRequest;
import com.example.demo.dto.mapper.UserEntityToUserResponseMapper;
import com.example.demo.exception.AccessDeniedException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.Roles;
import com.example.demo.model.ToDoEntity;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    private UserService userService;

    private UserRepository userRepository;

    private ToDoRepository toDoRepository;

    @BeforeEach
    public void setUp() {
        this.userRepository = mock(UserRepository.class);
        this.toDoRepository = mock(ToDoRepository.class);
        this.userService = new UserService(userRepository, toDoRepository);
    }

    @Test
    public void whenGetAll_thenReturnAll() {
        var users = new ArrayList<UserEntity>();
        users.add(new UserEntity(0L, "test", Roles.USER, Collections.emptyList()));
        users.add(new UserEntity(1L, "kirill", Roles.ADMIN, List.of(new ToDoEntity("test"))));
        when(userRepository.findAll()).thenReturn(users);

        var result = userService.getAll();

        assertEquals(users.size(), result.size());
        for (int i = 0; i < result.size(); i++) {
            assertThat(result.get(i), samePropertyValuesAs(
                    UserEntityToUserResponseMapper.map(users.get(i)))
            );
        }
    }

    @Test
    public void whenCreateUserThatNotExist_thenCreateUser() throws UserAlreadyExistsException, AccessDeniedException {
        var userEntity = new UserEntity(1L, "kandel_k", Roles.USER, null);
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.USER;
        saveRequest.username = "kandel_k";

        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(userEntity);

        var result = userService.create(saveRequest);

        assertEquals(saveRequest.username, result.username);
        assertEquals(saveRequest.role, result.role);
        assertNotNull(result.id);
    }

    @Test
    public void whenCreateUserThatExists_thenExistsException() {
        var user = new UserEntity();
        var saveRequest = new UserSaveRequest();
        saveRequest.username = "kandel_k";
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

        assertThrows(UserAlreadyExistsException.class, () -> userService.create(saveRequest));
    }

    @Test
    public void whenCreateAdminByPersonThatNotExists_thenNotFoundException() {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(AccessDeniedException.class, () -> userService.create(saveRequest));
    }

    @Test
    public void whenCreateAdminByUser_thenAccessException() {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        var user = new UserEntity();
        user.setRole(Roles.USER);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        assertThrows(AccessDeniedException.class, () -> userService.create(saveRequest));
    }

    @Test
    public void whenCreateAdminByAdmin_thenCreateAdmin() throws UserAlreadyExistsException, AccessDeniedException {
        var saveRequest = new UserSaveRequest();
        saveRequest.role = Roles.ADMIN;
        saveRequest.username = "kandel_k";
        saveRequest.creatorId = 0L;
        var userEntity = new UserEntity(1L, "kandel_k", Roles.ADMIN, null);
        var admin = new UserEntity();
        admin.setRole(Roles.ADMIN);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(admin));
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(userEntity);

        var userResponse = userService.create(saveRequest);

        assertEquals(saveRequest.username, userResponse.username);
        assertEquals(saveRequest.role, userResponse.role);
        assertNotNull(userResponse.id);
    }

    @Test
    public void whenGetOneWithCorrectId_thenReturnOne() throws UserNotFoundException {
        var user = new UserEntity(0L, "kandel_k", Roles.USER, null);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        var response = userService.getOne(0L);

        assertThat(response, samePropertyValuesAs(UserEntityToUserResponseMapper.map(user)));
    }

    @Test
    public void whenGetOneWithWrongId_thenNotFoundException() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.getOne(0L));
    }

    @Test
    public void whenUserDeleteHimself_thenDeleteUser() throws AccessDeniedException {
        var id = 0L;
        userService.deleteOne(id, id);
        verify(userRepository, times(1)).deleteById(id);
    }

    @Test
    public void whenUserDeleteNotHimself_thenAccessException() {
        var user = new UserEntity();
        user.setRole(Roles.USER);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        assertThrows(AccessDeniedException.class, () -> userService.deleteOne(0L, 1L));
    }

    @Test
    public void whenAdminDelete_thenDelete() throws AccessDeniedException {
        var user = new UserEntity();
        user.setRole(Roles.ADMIN);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));

        userService.deleteOne(0L, 1L);

        verify(userRepository, times(1)).deleteById(1L);
    }

    @Test
    public void whenAddTaskToUnexistingUser_thenUserNotFoundException() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.addTask(0L, 0L, 1L));
    }

    @Test
    public void whenAddUnexistingTask_thenToDoNotFoundException() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(new UserEntity()));
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ToDoNotFoundException.class, () -> userService.addTask(0L, 0L, 1L));
    }

    @Test
    public void whenUserReassignTask_thenAccessException() {
        var user = new UserEntity();
        user.setRole(Roles.USER);
        var todo = new ToDoEntity();
        todo.setUser(new UserEntity());
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(todo));

        assertThrows(AccessDeniedException.class, () -> userService.addTask(0L, 0L, 1L));
    }

    @Test
    public void whenUserAssignFreeTask_thenAddTaskToUser() throws UserNotFoundException, ToDoNotFoundException, AccessDeniedException {
        var user = new UserEntity(0L, "kandel_k", Roles.USER, new ArrayList<>());
        var task = new ToDoEntity("Test");
        var savedUser = new UserEntity(1L, "kandel_k", Roles.USER, new ArrayList<>());
        savedUser.getTasks().add(task);

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(task));
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(savedUser);

        var response = userService.addTask(0L, 0L, 1L);

        assertNotNull(response.id);
        assertEquals(user.getRole(), response.role);
        assertEquals(user.getUsername(), response.username);
        assertSame(task, response.tasks.get(0));
    }

    @Test
    public void whenAdminReassignTask_thenAssignTaskToNewUser() throws UserNotFoundException, ToDoNotFoundException, AccessDeniedException {
        var user = new UserEntity(1L, "user", Roles.USER, new ArrayList<>());
        var task = new ToDoEntity("Test");
        task.setUser(new UserEntity());
        var admin = new UserEntity(0L, "admin", Roles.ADMIN, new ArrayList<>());
        var savedUser = new UserEntity(1L, "user", Roles.USER, new ArrayList<>());
        savedUser.getTasks().add(task);

        when(userRepository.findById(0L)).thenReturn(Optional.of(admin));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.of(task));
        when(userRepository.save(ArgumentMatchers.any(UserEntity.class))).thenReturn(savedUser);

        var response = userService.addTask(0L, 1L, 2L);

        assertNotNull(response.id);
        assertEquals(user.getRole(), response.role);
        assertEquals(user.getUsername(), response.username);
        assertSame(task, response.tasks.get(0));
    }
}
