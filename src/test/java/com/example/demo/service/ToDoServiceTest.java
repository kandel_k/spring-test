package com.example.demo.service;

import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.mockito.Mockito.*;

public class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenDeleteCompletedTasks_thenRepositoryDeleteCalled() {
        var date = ZonedDateTime.now(ZoneOffset.UTC);
        toDoService.deleteCompletedBefore(date);

        verify(toDoRepository, times(1)).deleteByCompletedAtBefore(date);
    }
}
