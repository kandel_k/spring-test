package com.example.demo.dto;

import com.example.demo.model.Roles;
import com.example.demo.model.ToDoEntity;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UserResponse {

    @NotNull
    public Long id;

    @NotNull
    public String username;

    @NotNull
    public Roles role;

    public List<ToDoEntity> tasks;

    public UserResponse() {
    }

    public UserResponse(@NotNull Long id, @NotNull String username, @NotNull Roles role, List<ToDoEntity> tasks) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.tasks = tasks;
    }


}
