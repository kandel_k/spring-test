package com.example.demo.dto.mapper;

import com.example.demo.dto.UserResponse;
import com.example.demo.model.UserEntity;

public class UserEntityToUserResponseMapper {
    public static UserResponse map(UserEntity user) {
        if (user == null) {
            return null;
        }
        var response = new UserResponse();
        response.id = user.getId();
        response.role = user.getRole();
        response.tasks = user.getTasks();
        response.username = user.getUsername();

        return response;
    }
}
