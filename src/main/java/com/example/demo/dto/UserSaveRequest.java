package com.example.demo.dto;

import com.example.demo.model.Roles;

import javax.validation.constraints.NotNull;

public class UserSaveRequest {

    @NotNull
    public String username;

    public Roles role;

    public Long creatorId;
}
