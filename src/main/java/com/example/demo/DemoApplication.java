package com.example.demo;

import com.example.demo.model.Roles;
import com.example.demo.model.ToDoEntity;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.Collections;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    @Profile("demo")
    CommandLineRunner initDatabase(ToDoRepository repository, UserRepository userRepository) {
        return args -> {
            var user = new UserEntity(100L, "kandel_k", Roles.USER, Collections.emptyList());
            userRepository.save(user);
            repository.save(new ToDoEntity("Wash the dishes"));
            repository.save(
                    new ToDoEntity("Learn to test Java app").completeNow()
            );
        };
    }
}
