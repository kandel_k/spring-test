package com.example.demo.repository;

import com.example.demo.model.ToDoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {
    @Modifying
    @Transactional
    void deleteByCompletedAtBefore(ZonedDateTime now);
}
