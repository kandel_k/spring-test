package com.example.demo.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class UserEntity {

    @Id
    @NotNull
    @GeneratedValue
    private Long id;

    @Basic
    private String username;

    @Enumerated(EnumType.ORDINAL)

    private Roles role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.REFRESH)
    private List<ToDoEntity> tasks;

    public UserEntity() {
    }

    public UserEntity(@NotNull Long id, String username, Roles role, List<ToDoEntity> tasks) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.tasks = tasks;
    }

    public UserEntity(String username, Roles role) {
        this.username = username;
        this.role = role != null ? role : Roles.USER;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Roles getRole() {
        return role;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public List<ToDoEntity> getTasks() {
        return tasks;
    }

    public void setTasks(List<ToDoEntity> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%d, username='%s', role='%s']",
                id, username, role.toString()
        );
    }
}
