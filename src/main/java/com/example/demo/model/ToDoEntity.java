package com.example.demo.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * ToDoEntity
 */
@Entity
public class ToDoEntity {

	@Id
	@NotNull
	@GeneratedValue
	private Long id;

	@Basic
	@NotNull
	private String text;

	@Basic
	private ZonedDateTime completedAt;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public ToDoEntity() {}

	public ToDoEntity(String text) {
		this.text = text;
	}

	public ToDoEntity(Long id, String text) {
		this.id = id;
		this.text = text;
	}

	public ToDoEntity(Long id, String text, ZonedDateTime completedAt) {
		this.id = id;
		this.text = text;
		this.completedAt = completedAt;
	}

	@Override
	public String toString() {
		return String.format(
			"ToDoEntity[id=%d, text='%s', completedAt='%s']",
			id, text, completedAt.toString()
		);
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public ToDoEntity setText(String text) {
		this.text = text;
		return this;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public UserEntity getUser() {
		return user;
	}

	public ZonedDateTime getCompletedAt() {
		return completedAt;
	}
	public ToDoEntity completeNow() {
		completedAt = ZonedDateTime.now(ZoneOffset.UTC);
		user = null;
		return this;
	}
}
