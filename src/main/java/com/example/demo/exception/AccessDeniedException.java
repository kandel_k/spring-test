package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "You are not allowed to do that")
public class AccessDeniedException extends Exception {

    public AccessDeniedException() {super("You are not allowed to do that");}
}
