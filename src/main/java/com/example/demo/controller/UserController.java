package com.example.demo.controller;

import com.example.demo.dto.UserResponse;
import com.example.demo.dto.UserSaveRequest;
import com.example.demo.exception.AccessDeniedException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    @Valid
    public List<UserResponse> getAll() {
        return userService.getAll();
    }

    @PostMapping("/users")
    @Valid
    public UserResponse save(@Valid @RequestBody UserSaveRequest saveRequest)
            throws UserAlreadyExistsException, AccessDeniedException {
        return userService.create(saveRequest);
    }

    @PutMapping("/user/{id}")
    @Valid
    public UserResponse addTask(@PathVariable Long id, Long ownerId, Long taskId)
            throws UserNotFoundException, ToDoNotFoundException, AccessDeniedException {
        return userService.addTask(ownerId, id, taskId);
    }

    @GetMapping("/user/{id}")
    public UserResponse getOne(@PathVariable Long id) throws UserNotFoundException {
        return userService.getOne(id);
    }

    @DeleteMapping("/user/{id}")
    public void delete(Long ownerId, @PathVariable Long id) throws AccessDeniedException {
        userService.deleteOne(ownerId, id);
    }
}
