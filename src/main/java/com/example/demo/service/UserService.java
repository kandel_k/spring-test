package com.example.demo.service;

import com.example.demo.dto.UserResponse;
import com.example.demo.dto.UserSaveRequest;
import com.example.demo.dto.mapper.UserEntityToUserResponseMapper;
import com.example.demo.exception.AccessDeniedException;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.Roles;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final ToDoRepository toDoRepository;

    public UserService(UserRepository userRepository, ToDoRepository toDoRepository) {
        this.userRepository = userRepository;
        this.toDoRepository = toDoRepository;
    }

    private boolean isUserHasRoleUser(Long id) throws AccessDeniedException {
        return userRepository.findById(id)
                .orElseThrow(AccessDeniedException::new)
                .getRole() != Roles.ADMIN;
    }

    public List<UserResponse> getAll() {
        return userRepository.findAll().stream()
                .map(UserEntityToUserResponseMapper::map)
                .collect(Collectors.toList());
    }

    // Only admin can create admin, and user can create only himself
    public UserResponse create(UserSaveRequest request) throws UserAlreadyExistsException, AccessDeniedException {

        if (userRepository.findByUsername(request.username).isPresent()) {
            throw new UserAlreadyExistsException(request.username);
        }
        if (request.role == Roles.ADMIN && isUserHasRoleUser(request.creatorId)) {
            throw new AccessDeniedException();
        }

        var user = new UserEntity(request.username, request.role);
        return UserEntityToUserResponseMapper.map(userRepository.save(user));
    }

    // Only admin can reassign task to another user and user can assign empty task
    public UserResponse addTask(Long requestOwnerId, Long addToUserId, Long taskId)
            throws ToDoNotFoundException, UserNotFoundException, AccessDeniedException {

        var user = userRepository.findById(addToUserId).orElseThrow(() -> new UserNotFoundException(addToUserId));
        var task = toDoRepository.findById(taskId).orElseThrow(() -> new ToDoNotFoundException(taskId));

        if (task.getUser() != null && isUserHasRoleUser(requestOwnerId)) {
            throw new AccessDeniedException();
        }

        var userTasks = user.getTasks();
        userTasks.add(task);

        return UserEntityToUserResponseMapper.map(userRepository.save(user));
    }

    public UserResponse getOne(Long id) throws UserNotFoundException {
        return UserEntityToUserResponseMapper.map(
                userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id))
        );
    }

    // Admin can delete everybody but user can only delete himself
    public void deleteOne(Long requestOwnerId, Long id) throws AccessDeniedException {
        if (!requestOwnerId.equals(id) && isUserHasRoleUser(requestOwnerId)) {
            throw new AccessDeniedException();
        }
        userRepository.deleteById(id);
    }
}
